package com.namaztime.namaztime.database

import android.content.Context
import android.content.SharedPreferences
import com.namaztime.namaztime.util.StaticKey

class MySharedPreparence {

    private var context: Context? = null
    var preferences: SharedPreferences? = null
    private val SHARED_PREFERENCE_NAME = "mysharedprefarence"
    private val MADHAB = "madab"
    private val ALL_ALARM = "allalarm"
    private val FAJR = "fajr"
    private val DHUHR = "dhuhr"
    private val ASR = "asr"
    private val MAGRIB = "magrib"
    private val SUN_SET = "sunset"
    private val ISHA = "isha"
    private val AUTO_START_PERMISSION = "autostartpermission"
    private val LATITUDE = "latitude"
    private val LONGITUDE = "longitude"
    private val LANGUAGE_TYPE = "languagetype"


    constructor(context: Context) {
        preferences = context.getSharedPreferences(SHARED_PREFERENCE_NAME, Context.MODE_PRIVATE)
        this.context = context
    }

    fun setFajr(value: Boolean) {
        val editor = preferences!!.edit()
        editor.putBoolean(FAJR, value)
        editor.commit()
    }

    fun isAlarmForFajr(): Boolean {
        return preferences!!.getBoolean(FAJR, false)
    }

    fun setDhuhr(value: Boolean) {
        val editor = preferences!!.edit()
        editor.putBoolean(DHUHR, value)
        editor.commit()
    }

    fun isAlarmForDhuhr(): Boolean {
        return preferences!!.getBoolean(DHUHR, false)
    }

    fun setAsr(value: Boolean) {
        val editor = preferences!!.edit()
        editor.putBoolean(ASR, value)
        editor.commit()
    }

    fun isAlarmForAsr(): Boolean {
        return preferences!!.getBoolean(ASR, false)
    }

    fun setMagrib(value: Boolean) {
        val editor = preferences!!.edit()
        editor.putBoolean(MAGRIB, value)
        editor.commit()
    }

    fun isAlarmForMagrib(): Boolean {
        return preferences!!.getBoolean(MAGRIB, false)
    }

    fun setSunset(value: Boolean) {
        val editor = preferences!!.edit()
        editor.putBoolean(SUN_SET, value)
        editor.commit()
    }

    fun isAlarmForSunset(): Boolean {
        return preferences!!.getBoolean(SUN_SET, false)
    }

    fun setIsha(value: Boolean) {
        val editor = preferences!!.edit()
        editor.putBoolean(ISHA, value)
        editor.commit()
    }

    fun isAlarmForIsha(): Boolean {
        return preferences!!.getBoolean(ISHA, false)
    }

    fun setAutoStartPermission(value: Boolean) {
        val editor = preferences!!.edit()
        editor.putBoolean(AUTO_START_PERMISSION, value)
        editor.commit()
    }

    fun isAutoStartPermissionGranted(): Boolean {
        return preferences!!.getBoolean(AUTO_START_PERMISSION, false)
    }


    fun setLatitude(value: String) {
        val editor = preferences!!.edit()
        editor.putString(LATITUDE, value)
        editor.commit()
    }

    fun getLatitude(): String? {

        return preferences!!.getString(LATITUDE, "23.8103");
    }

    fun setLongitude(value: String) {
        val editor = preferences!!.edit()
        editor.putString(LONGITUDE, value)
        editor.commit()
    }

    fun getLongitude(): String? {

        return preferences!!.getString(LONGITUDE, "90.4125");
    }

    fun setLanguage(value: String) {
        val editor = preferences!!.edit()
        editor.putString(LANGUAGE_TYPE, value)
        editor.commit()
    }

    fun getLanguage(): String? {
        return preferences!!.getString(LANGUAGE_TYPE, StaticKey().BANGLA);
    }

    fun setAllAlarm(value: Boolean) {
        val editor = preferences!!.edit()
        editor.putBoolean(ALL_ALARM, value)
        editor.commit()
    }

    fun getAllAlarm(): Boolean? {
        return preferences!!.getBoolean(ALL_ALARM, false);
    }

    fun setMadhab(value: String) {
        val editor = preferences!!.edit()
        editor.putString(MADHAB, value)
        editor.commit()
    }

    fun getMadhab(): String? {
        return preferences!!.getString(MADHAB, StaticKey().HANAFI)
    }
}
