package com.namaztime.namaztime.view

import android.content.res.Configuration
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import java.util.*


open class BaseActivity : AppCompatActivity() {
    protected fun setLocalLanguage(type: String?) {
        val locale = Locale(type)
        Locale.setDefault(locale)
        val configuration: Configuration = resources.configuration
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.N) {
            configuration.setLocale(locale)
            createConfigurationContext(configuration)
        }
        configuration.locale = locale
        configuration.setLayoutDirection(locale)
        resources.updateConfiguration(configuration, resources.displayMetrics)
    }
}