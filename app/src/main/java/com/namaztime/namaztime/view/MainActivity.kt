package com.namaztime.namaztime.view

import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.CountDownTimer
import android.os.PowerManager
import android.provider.Settings
import android.util.Log
import android.view.View
import android.view.Window
import android.view.WindowManager
import androidx.appcompat.app.AlertDialog
import androidx.databinding.DataBindingUtil
import com.azan.astrologicalCalc.SimpleDate
import com.batoulapps.adhan.PrayerTimes
import com.github.msarhan.ummalqura.calendar.UmmalquraCalendar
import com.namaztime.namaztime.R
import com.namaztime.namaztime.database.MySharedPreparence
import com.namaztime.namaztime.databinding.ActivityMainBinding
import com.namaztime.namaztime.util.*
import mumayank.com.airlocationlibrary.AirLocation
import java.text.SimpleDateFormat
import java.util.*


class MainActivity : BaseActivity() {
    private var mWindow: Window? = null
    var binding: ActivityMainBinding? = null
    var preparence: MySharedPreparence? = null

    private val MY_IGNORE_OPTIMIZATION_REQUEST = 12

    val dates = SimpleDateFormat("MM/dd/yyyy hh:mm:ss a")
    var namazTimeToday: PrayerTimes? = null
    var namazTimeTomorrow: PrayerTimes? = null
    var countDownTimer: CountDownTimer? = null
    var namazAlarm: NamazAlarm? = null

    private var airLocation: AirLocation? = null
    var lat: Double = 0.0
    var lon: Double = 0.0


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val w = window
        w.setFlags(
            WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS,
            WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS
        )
        binding = DataBindingUtil.setContentView(
            this,
            R.layout.activity_main
        )
        w.getDecorView().setSystemUiVisibility(
            View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                    or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
        )
        preparence = MySharedPreparence(this)
        setLocalLanguage(preparence?.getLanguage())
        init()

        binding?.imageViewSettings?.setOnClickListener {
            startActivity(
                Intent(this, SettingsActivity::class.java)
                // .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
            )
            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left)
            finish()

        }
        binding?.llFajr?.setOnClickListener {
            if (preparence?.isAlarmForFajr()!!) {
                binding?.imageViewFajr?.setImageResource(R.drawable.alarm_mute)
                namazAlarm?.cancelAlarmForFojor()
            } else {
                binding?.imageViewFajr?.setImageResource(R.drawable.alarm_immute)
                NamazAlarm(this).setAlarmForFojor(
                    if (namazTimeToday?.fajr!! > Date()) {
                        true
                    } else {
                        false
                    }
                )
            }
        }
        binding?.llJohor?.setOnClickListener {
            if (preparence?.isAlarmForDhuhr()!!) {
                binding?.imageViewJohor?.setImageResource(R.drawable.alarm_mute)
                namazAlarm?.cancelAlarmForJohor()
            } else {
                binding?.imageViewJohor?.setImageResource(R.drawable.alarm_immute)
                namazAlarm?.setAlarmForJohor(
                    if (namazTimeToday?.dhuhr!! > Date()) {
                        true
                    } else {
                        false
                    }
                )
            }
        }
        binding?.llAsr?.setOnClickListener {
            if (preparence?.isAlarmForAsr()!!) {
                binding?.imageViewAsr?.setImageResource(R.drawable.alarm_mute)
                namazAlarm?.cancelAlarmForAsr()
            } else {
                namazAlarm?.setAlarmForAsr(
                    if (namazTimeToday?.asr!! > Date()) {
                        true
                    } else {
                        false
                    }
                )
                binding?.imageViewAsr?.setImageResource(R.drawable.alarm_immute)
            }
        }
        binding?.llSunset?.setOnClickListener {
            if (preparence?.isAlarmForSunset()!!) {
                binding?.imageViewSunset?.setImageResource(R.drawable.alarm_mute)
                namazAlarm?.cancelAlarmForSunSet()
            } else {
                binding?.imageViewSunset?.setImageResource(R.drawable.alarm_immute)
                namazAlarm?.setAlarmForSunSet(
                    if (namazTimeToday?.maghrib!! > Date()) {
                        true
                    } else {
                        false
                    }
                )
            }
        }
        binding?.llMagrib?.setOnClickListener {
            if (preparence?.isAlarmForMagrib()!!) {
                binding?.imageViewMagrib?.setImageResource(R.drawable.alarm_mute)
                namazAlarm?.cancelAlarmForMagrib()
            } else {
                binding?.imageViewMagrib?.setImageResource(R.drawable.alarm_immute)
                namazAlarm?.setAlarmForMagribPrayer(
                    if (namazTimeToday?.maghrib!! > Date()) {
                        true
                    } else {
                        false
                    }
                )
            }
        }
        binding?.llIsha?.setOnClickListener {
            if (preparence?.isAlarmForIsha()!!) {
                binding?.imageViewIsha?.setImageResource(R.drawable.alarm_mute)
                namazAlarm?.cancelAlarmForIsha()
            } else {
                binding?.imageViewIsha?.setImageResource(R.drawable.alarm_immute)
                namazAlarm?.setAlarmForIsha(
                    if (namazTimeToday?.isha!! > Date()) {
                        true
                    } else {
                        false
                    }
                )
            }
        }
        namazAlarm?.refreshAllSelectedAlarm("none")
        deviceLocation()
        batteryoptimization()
        setPrayerTime()
        showNextPrayerTime()
        setData()
    }

    private fun deviceLocation() {
        airLocation = AirLocation(this, true, true, object : AirLocation.Callbacks {
            override fun onSuccess(location: android.location.Location) {
                if (location != null) {
                    lat = location.latitude
                    lon = location.longitude
                    preparence?.setLatitude(lat.toString())
                    preparence?.setLongitude(lon.toString())
                    setPrayerTime()
                    Log.d("message", "lat ${lat} longi ${lon}")
                }
            }

            override fun onFailed(locationFailedEnum: AirLocation.LocationFailedEnum) {
                Log.d("message", "onFailed")
            }

        })

    }

    override fun onResume() {
        super.onResume()
        countDownTimer = object : CountDownTimer(30000, 10000) {
            override fun onTick(millisUntilFinished: Long) {
                showNextPrayerTime()
            }

            override fun onFinish() {
                countDownTimer?.start()
            }
        };
        countDownTimer?.start()
    }

    override fun onStop() {
        super.onStop()
        countDownTimer?.cancel();
    }

    fun init() {
        namazTimeToday = NamazTime(this).namazeTimeToday()
        namazTimeTomorrow = NamazTime(this).namazeTimeTomorrow()
        namazAlarm = NamazAlarm(this)
    }

    fun setData() {
        if (preparence?.isAlarmForFajr()!!) {
            binding?.imageViewFajr?.setImageResource(R.drawable.alarm_immute)
        } else {
            binding?.imageViewFajr?.setImageResource(R.drawable.alarm_mute)
        }
        if (preparence?.isAlarmForDhuhr()!!) {
            binding?.imageViewJohor?.setImageResource(R.drawable.alarm_immute)
        } else {
            binding?.imageViewJohor?.setImageResource(R.drawable.alarm_mute)
        }
        if (preparence?.isAlarmForAsr()!!) {
            binding?.imageViewAsr?.setImageResource(R.drawable.alarm_immute)
        } else {
            binding?.imageViewAsr?.setImageResource(R.drawable.alarm_mute)
        }
        if (preparence?.isAlarmForSunset()!!) {
            binding?.imageViewSunset?.setImageResource(R.drawable.alarm_immute)
        } else {
            binding?.imageViewSunset?.setImageResource(R.drawable.alarm_mute)
        }
        if (preparence?.isAlarmForMagrib()!!) {
            binding?.imageViewMagrib?.setImageResource(R.drawable.alarm_immute)
        } else {
            binding?.imageViewMagrib?.setImageResource(R.drawable.alarm_mute)
        }
        if (preparence?.isAlarmForIsha()!!) {
            binding?.imageViewIsha?.setImageResource(R.drawable.alarm_immute)
        } else {
            binding?.imageViewIsha?.setImageResource(R.drawable.alarm_mute)
        }
    }

    fun showNextPrayerTime() {
        val dates = SimpleDateFormat("MM/dd/yyyy hh:mm:ss a")
        when (namazTimeToday?.nextPrayer().toString().toLowerCase()) {
            "fajr" -> {
                var initialDelay: String = DateConverter().getTimeInHoursMinutes(
                    dates.format(Date()),
                    dates.format(namazTimeToday?.fajr), preparence
                ).toString()

                binding?.tvNextPrayer?.setText(
                    "${getString(R.string.fojor_time_will_be_start)} ${initialDelay} ${getString(
                        R.string.later
                    )} "
                )
            }
            "dhuhr" -> {
                var initialDelay: String = DateConverter().getTimeInHoursMinutes(
                    dates.format(Date()),
                    dates.format(namazTimeToday?.dhuhr), preparence
                ).toString()
                binding?.tvNextPrayer?.setText(
                    "${getString(R.string.johor_time_will_be_start)} ${initialDelay} ${getString(
                        R.string.later
                    )}"
                )

            }
            "asr" -> {
                var initialDelay: String = DateConverter().getTimeInHoursMinutes(
                    dates.format(Date()),
                    dates.format(namazTimeToday?.asr), preparence
                ).toString()
                binding?.tvNextPrayer?.setText(
                    "${getString(R.string.asr_time_will_be_start)} ${initialDelay} ${getString(
                        R.string.later
                    )} "
                )
            }
            "maghrib" -> {
                var initialDelay: String = DateConverter().getTimeInHoursMinutes(
                    dates.format(Date()),
                    dates.format(namazTimeToday?.maghrib), preparence
                ).toString()


                //  Log.e("message", "Next prayer ${namazTimeToday.nextPrayer(namazTimeToday.maghrib)}")
                binding?.tvNextPrayer?.setText(
                    "${getString(R.string.magrib_time_will_be_start)} ${initialDelay} ${getString(
                        R.string.later
                    )} "
                )
            }
            "isha" -> {
                var initialDelay: String = DateConverter().getTimeInHoursMinutes(
                    dates.format(Date()),
                    dates.format(namazTimeToday?.isha), preparence
                ).toString()


                //  Log.e("message", "Next prayer ${namazTimeToday.nextPrayer(namazTimeToday.maghrib)}")
                binding?.tvNextPrayer?.setText(
                    "${getString(R.string.eshaa_time_will_be_start)} ${initialDelay} ${getString(
                        R.string.later
                    )} "
                )

            }
            "none" -> {
                var initialDelay: String = DateConverter().getTimeInHoursMinutes(
                    dates.format(Date()),
                    dates.format(namazTimeTomorrow?.fajr), preparence
                ).toString()
                //  Log.e("message", "Next prayer ${namazTimeToday.nextPrayer(namazTimeToday.maghrib)}")
                binding?.tvNextPrayer?.setText(
                    "${getString(R.string.tomorrow_fojor_time_wil_be_start)} ${initialDelay} ${getString(
                        R.string.later
                    )} "
                )
            }
        }
    }

    fun setPrayerTime() {
        val today = SimpleDate(GregorianCalendar())
        val fromServer = SimpleDateFormat("yyyy-MM-dd'T'")
        val myFormat = SimpleDateFormat("yyyy-M-dd'T'")
        val inputDateStr = "" + today.year + "-" + today.month + "-" + today.day + "T"
        val outputDateStr = myFormat.parse(inputDateStr)
        val str: Array<String> = outputDateStr.toString().split(" ".toRegex()).toTypedArray()
        val newFormate = SimpleDateFormat("h:mm a")
        if (preparence?.getLanguage().equals(StaticKey().BANGLA))
            binding?.tvDate?.setText(
                "${Converter().getDayInBangla(str[0])}, ${Converter().getMonthInBangla(str[1])}" +
                        " ${Converter().numberEnglishToBanglaConvert(str[2].toInt())}, ${
                        Converter().numberEnglishToBanglaConvert(
                            str[str.size - 1].toInt()
                        )
                        }"
            )
        else
            binding?.tvDate?.setText(
                "${str[0]}, ${str[1]}" +
                        " ${str[2].toInt()}, ${
                        str[str.size - 1]
                        }"
            )
        val namazTimeToday: PrayerTimes = NamazTime(this)
            .namazeTimeToday()
        binding?.tvFojor?.setText("${getString(R.string.fojor)}")
        binding?.tvFojorTime?.setText("" + newFormate.format(namazTimeToday.fajr))
        binding?.tvJohor?.setText("${getString(R.string.johor)}")
        binding?.tvjohorTime?.setText("" + newFormate.format(namazTimeToday.dhuhr))
        binding?.tvAsor?.setText("${getString(R.string.asor)}")
        binding?.tvAsorTime?.setText("" + newFormate.format(namazTimeToday.asr))
        binding?.tvSunset1?.setText("${getString(R.string.sunset)}")
        binding?.tvSunsetTime?.setText("" + newFormate.format(namazTimeToday.maghrib))
        binding?.tvMagrib?.setText("${getString(R.string.magrib)}")
        binding?.tvMagribTime?.setText("" + newFormate.format(namazTimeToday.maghrib))
        binding?.tvIsha?.setText("${getString(R.string.eshaa)}")
        binding?.tvEshaaTime?.setText("" + newFormate.format(namazTimeToday.isha))
        var cal = UmmalquraCalendar()
        cal.get(Calendar.YEAR)         // 1436
        cal.get(Calendar.MONTH)      // 5 <=> Jumada al-Akhirah
        cal.get(Calendar.ZONE_OFFSET)
        if (preparence?.getLanguage().equals(StaticKey().BANGLA))
            binding?.tvHijriDate?.setText(
                "${Converter().numberEnglishToBanglaConvert(cal.get(Calendar.DAY_OF_MONTH))}, " +
                        "${Converter().getArabicMonthNameInBangla(cal.get(Calendar.MONTH))}, " +
                        "${Converter().numberEnglishToBanglaConvert(cal.get(Calendar.YEAR))} ${getString(
                            R.string.hijri
                        )}  "
            )
        else {
            binding?.tvHijriDate?.setText(
                "${Calendar.DAY_OF_MONTH}, " +
                        "${Converter().getArabicMonthNameInEnglish(cal.get(Calendar.MONTH))}, " +
                        "${cal.get(Calendar.YEAR)} ${getString(R.string.hijri)}"
            )
        }
    }

    private fun batteryoptimization() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            val intent = Intent()
            val packageName = packageName
            val pm = getSystemService(POWER_SERVICE) as PowerManager
            if (!pm.isIgnoringBatteryOptimizations(packageName)) {
                intent.action = Settings.ACTION_REQUEST_IGNORE_BATTERY_OPTIMIZATIONS
                intent.data = Uri.parse("package:$packageName")
                startActivityForResult(intent, MY_IGNORE_OPTIMIZATION_REQUEST)
            } else {
                enableAutoStart()
            }
        }
    }

    private fun enableAutoStart() {
        val POWERMANAGER_INTENTS = Permission.pawerManagerIntents()
        for (intent in POWERMANAGER_INTENTS) if (packageManager.resolveActivity(
                intent,
                PackageManager.MATCH_DEFAULT_ONLY
            ) != null
        ) {
            if (!preparence?.isAutoStartPermissionGranted()!!) {
                // show dialog to ask user action
                val builder = AlertDialog.Builder(this)
                builder.setTitle("Do You Want To Give Auto Start Permission")
                    .setPositiveButton("Yes") { dialogInterface, i ->
                        try {
                            startActivityForResult(
                                intent,
                                2
                            )
                        } catch (e: Exception) {
                        }

                    }.setNegativeButton(
                        "No"
                    )
                    { dialogInterface, i -> }.show()
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == MY_IGNORE_OPTIMIZATION_REQUEST) {
            val pm = getSystemService(POWER_SERVICE) as PowerManager
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                val isIgnoringBatteryOptimizations = pm.isIgnoringBatteryOptimizations(
                    packageName
                )
                enableAutoStart()
                if (isIgnoringBatteryOptimizations) {
                } else {
                }
            }
        } else if (requestCode == 2) {
            preparence?.setAutoStartPermission(true)
        }
    }
}