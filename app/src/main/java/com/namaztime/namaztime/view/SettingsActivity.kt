package com.namaztime.namaztime.view

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.view.MenuItem
import androidx.databinding.DataBindingUtil
import com.batoulapps.adhan.PrayerTimes
import com.namaztime.namaztime.R
import com.namaztime.namaztime.database.MySharedPreparence
import com.namaztime.namaztime.databinding.ActivitySettingsBinding
import com.namaztime.namaztime.util.NamazAlarm
import com.namaztime.namaztime.util.NamazTime
import com.namaztime.namaztime.util.StaticKey
import java.util.*


class SettingsActivity : BaseActivity() {

    var namazTimeToday: PrayerTimes? = null
    var preparence: MySharedPreparence? = null
    var binding: ActivitySettingsBinding? = null
    var namazTime: NamazAlarm? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_settings)
        preparence = MySharedPreparence(this)
        setLocalLanguage(preparence?.getLanguage())
        namazTime = NamazAlarm(this)
        namazTimeToday = NamazTime(this).namazeTimeToday()


        binding?.englishTV?.setOnClickListener {
            setEnglish()
        }
        binding?.banglaTV?.setOnClickListener {
            setBangla()
        }
        binding?.hanafiTv?.setOnClickListener {
            setHanafi()
        }
        binding?.shapiTv?.setOnClickListener {
            setShafi()
        }
        binding?.onTv?.setOnClickListener {
            setAlarmOnView()
            setAllAlarm()
        }
        binding?.offTv?.setOnClickListener {
            setAlarmOffView()
            cancelAllAlarm()
        }
        setSettingsValue()

    }

    fun setSettingsValue() {
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setTitle(getString(R.string.settings))
        if (preparence?.getMadhab().equals(StaticKey().HANAFI)) {
            setHanafi()
        } else {
            setShafi()
        }
        if (preparence?.getAllAlarm()!!) {
            setAlarmOnView()
        } else {
            if (preparence?.isAlarmForFajr()!! && preparence?.isAlarmForDhuhr()!! && preparence?.isAlarmForAsr()!! && preparence?.isAlarmForSunset()!!
                && preparence?.isAlarmForMagrib()!! && preparence?.isAlarmForIsha()!!
            ) {
                setAlarmOnView()
            } else
                setAlarmOffView()
        }
        if (preparence?.getLanguage().equals(StaticKey().ENGLISH)) {
            setEnglish()
        } else {
            setBangla()
        }

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home ->
                onBackPressed()
        }
        return true;
    }

    fun setBangla() {
        preparence?.setLanguage(StaticKey().BANGLA)
        binding?.banglaTV?.setBackgroundResource(R.drawable.round_shape_bg_white)
        binding?.banglaTV?.setTextColor(Color.parseColor("#03DAC5"))
        binding?.englishTV?.setTextColor(Color.parseColor("#000000"))
        binding?.englishTV?.setBackgroundResource(0)
    }

    override fun onBackPressed() {
        startActivity(
            Intent(this, MainActivity::class.java)
                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
        )
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left)
    }

    fun setEnglish() {
        preparence?.setLanguage(StaticKey().ENGLISH)
        binding?.englishTV?.setBackgroundResource(R.drawable.round_shape_bg_white)
        binding?.banglaTV?.setBackgroundResource(0)
        binding?.englishTV?.setTextColor(Color.parseColor("#03DAC5"))
        binding?.banglaTV?.setTextColor(Color.parseColor("#000000"))
    }

    fun setHanafi() {
        preparence?.setMadhab(StaticKey().HANAFI)
        binding?.hanafiTv?.setBackgroundResource(R.drawable.round_shape_bg_white)
        binding?.hanafiTv?.setTextColor(Color.parseColor("#03DAC5"))
        binding?.shapiTv?.setTextColor(Color.parseColor("#000000"))
        binding?.shapiTv?.setBackgroundResource(0)

    }


    fun setShafi() {
        preparence?.setMadhab(StaticKey().SHAFI)
        binding?.shapiTv?.setBackgroundResource(R.drawable.round_shape_bg_white)
        binding?.hanafiTv?.setBackgroundResource(0)
        binding?.shapiTv?.setTextColor(Color.parseColor("#03DAC5"))
        binding?.hanafiTv?.setTextColor(Color.parseColor("#000000"))

    }

    fun setAlarmOnView() {

        binding?.onTv?.setBackgroundResource(R.drawable.round_shape_bg_white)
        binding?.onTv?.setTextColor(Color.parseColor("#03DAC5"))
        binding?.offTv?.setTextColor(Color.parseColor("#000000"))
        binding?.offTv?.setBackgroundResource(0)

    }

    private fun setAllAlarm() {
        preparence?.setAllAlarm(true)
        namazTime?.setAlarmForFojor(
            if (namazTimeToday?.dhuhr!! > Date()) {
                true
            } else {
                false
            }
        )
        namazTime?.setAlarmForJohor(
            if (namazTimeToday?.dhuhr!! > Date()) {
                true
            } else {
                false
            }
        )
        namazTime?.setAlarmForAsr(
            if (namazTimeToday?.asr!! > Date()) {
                true
            } else {
                false
            }
        )
        namazTime?.setAlarmForSunSet(
            if (namazTimeToday?.maghrib!! > Date()) {
                true
            } else {
                false
            }
        )
        namazTime?.setAlarmForMagribPrayer(
            if (namazTimeToday?.maghrib!! > Date()) {
                true
            } else {
                false
            }
        )
        namazTime?.setAlarmForIsha(
            if (namazTimeToday?.isha!! > Date()) {
                true
            } else {
                false
            }
        )
    }

    private fun cancelAllAlarm() {
        preparence?.setAllAlarm(false)
        namazTime?.cancelAlarmForFojor()
        namazTime?.cancelAlarmForJohor()
        namazTime?.cancelAlarmForAsr()
        namazTime?.cancelAlarmForSunSet()
        namazTime?.cancelAlarmForMagrib()
        namazTime?.cancelAlarmForIsha()
    }

    fun setAlarmOffView() {
        binding?.offTv?.setBackgroundResource(R.drawable.round_shape_bg_white)
        binding?.onTv?.setBackgroundResource(0)
        binding?.offTv?.setTextColor(Color.parseColor("#03DAC5"))
        binding?.onTv?.setTextColor(Color.parseColor("#000000"))

    }
}