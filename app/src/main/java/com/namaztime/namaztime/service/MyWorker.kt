package com.namaztime.namaztime.service

import android.app.Notification
import android.app.Notification.DEFAULT_ALL
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.NotificationManager.IMPORTANCE_HIGH
import android.app.PendingIntent
import android.app.PendingIntent.getActivity
import android.content.Context
import android.content.Context.NOTIFICATION_SERVICE
import android.content.Intent
import android.content.Intent.FLAG_ACTIVITY_CLEAR_TASK
import android.content.Intent.FLAG_ACTIVITY_NEW_TASK
import android.graphics.Color.RED
import android.media.AudioAttributes
import android.media.AudioAttributes.CONTENT_TYPE_SONIFICATION
import android.media.AudioAttributes.USAGE_NOTIFICATION_RINGTONE
import android.media.RingtoneManager.TYPE_NOTIFICATION
import android.media.RingtoneManager.getDefaultUri
import android.net.Uri
import android.os.Build
import android.os.Build.VERSION.SDK_INT
import android.os.Build.VERSION_CODES.O
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationCompat.PRIORITY_MAX
import androidx.core.app.NotificationManagerCompat
import androidx.work.*
import com.namaztime.namaztime.view.MainActivity
import com.namaztime.namaztime.R
import com.namaztime.namaztime.util.*
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit


class MyWorker(context: Context, workerParams: WorkerParameters) :
    Worker(context, workerParams) {
    val formatter = SimpleDateFormat("hh:mm a")
    var namazAlarm: NamazAlarm? = null
    override fun doWork(): Result {

        val inputData = inputData.getString(StaticKey().KEY)
        setNextAlarm(inputData.toString())

//        val data = Data.Builder()
//        data.putString(StaticKey().ASR, StaticKey().ASR)
//        val compressionWork = OneTimeWorkRequest.Builder(MyWorker::class.java)
//            .setInitialDelay(15, TimeUnit.SECONDS)
//            .addTag(StaticKey().ASR)
//            .setInputData(data.build())
//            .build()
//        WorkManager.getInstance(applicationContext).enqueue(compressionWork)
        return Result.success()
    }

    fun setNextAlarm(key: String) {
        val dates = SimpleDateFormat("MM/dd/yyyy hh:mm:ss a")
        val namazTimeToday = NamazTime(applicationContext).namazeTimeToday()
        val namazTimeTomorrow = NamazTime(applicationContext).namazeTimeTomorrow()
        var namazAlarm = NamazAlarm(applicationContext)

        when (key) {
            StaticKey().FAJAR -> {
                displayNotification(
                    applicationContext.getString(com.namaztime.namaztime.R.string.fojor),
                    formatter.format(namazTimeToday.fajr)
                )
                namazAlarm.setAlarmForFojor(false)
            }
            StaticKey().ZUHR -> {
                displayNotification(
                    applicationContext.getString(R.string.johor),
                    formatter.format(namazTimeToday.dhuhr)
                )
                namazAlarm.setAlarmForJohor(false)
            }
            StaticKey().ASR -> {
                displayNotification(
                    applicationContext.getString(R.string.asor),
                    formatter.format(namazTimeToday.asr)
                )
                namazAlarm.setAlarmForAsr(false)
            }
            StaticKey().SUNSET -> {
                displayNotification(
                    applicationContext.getString(R.string.sunset),
                    formatter.format(namazTimeToday.maghrib)
                )
                namazAlarm.setAlarmForSunSet(false)
            }
            StaticKey().MAGRIB -> {
                displayNotification(
                    applicationContext.getString(R.string.magrib),
                    formatter.format(namazTimeToday.maghrib)
                )
                namazAlarm.setAlarmForMagribPrayer(false)
            }
            StaticKey().ISHA -> {
                displayNotification(
                    applicationContext.getString(R.string.eshaa),
                    formatter.format(namazTimeToday.isha)
                )
                namazAlarm.setAlarmForIsha(false)
            }
        }

        namazAlarm.refreshAllSelectedAlarm(key)

    }


//     fun displayNotification(title: String, body: String?) {
//        val v = longArrayOf(500, 1000)
//        val soundUri =
//            Uri.parse("android.resource://" + applicationContext.packageName + "/" + com.namaztime.namaztime.R.raw.alarm)
//        val intent = Intent(applicationContext, MainActivity::class.java)
//        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
//        val pendingIntent = PendingIntent.getActivity(
//            applicationContext,
//            1,
//            intent,
//            PendingIntent.FLAG_UPDATE_CURRENT
//        )
//        val builder = NotificationCompat.Builder(
//            applicationContext,
//            applicationContext.getString(com.namaztime.namaztime.R.string.default_notification_channel_id)
//        )
//            .setSmallIcon(com.namaztime.namaztime.R.drawable.ringer)
//            .setContentTitle(title)
//            .setContentText(body)
//            .setAutoCancel(true)
//            .setContentIntent(pendingIntent)
//            .setVibrate(v)
//            .setSound(soundUri)
//
////        if (data.get("image") != null) {
////            builder.setStyle(new NotificationCompat.BigPictureStyle()
////                    .bigPicture(getBitmapFromUrl(data.get("image"))));
////        }
//        val manager =
//            applicationContext.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//            val channel = NotificationChannel(
//                applicationContext.getString(com.namaztime.namaztime.R.string.default_notification_channel_id),
//                "Default channel",
//                NotificationManager.IMPORTANCE_HIGH
//            )
//            if (soundUri != null) {
//                // Changing Default mode of notification
//                builder.setDefaults(Notification.DEFAULT_VIBRATE)
//                // Creating an Audio Attribute
//                val audioAttributes = AudioAttributes.Builder()
//                    .setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION)
//                    .setUsage(AudioAttributes.USAGE_ALARM)
//                    .build()
//                channel.setSound(soundUri, audioAttributes)
//            }
//            manager?.createNotificationChannel(channel)
//        }
//           manager?.notify(0, builder.build())
//       // NotificationManagerCompat.from(applicationContext).notify(0,builder.build())
//
////        NotificationManager notificationManager = (NotificationManager) getApplicationContext().getSystemService(NOTIFICATION_SERVICE);
////
////        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
////            NotificationChannel channel = new NotificationChannel("simplifiedcoding", "simplifiedcoding", NotificationManager.IMPORTANCE_DEFAULT);
////            notificationManager.createNotificationChannel(channel);
////        }
////
////        NotificationCompat.Builder notification = new NotificationCompat.Builder(getApplicationContext(), "simplifiedcoding")
////                .setContentTitle(title)
////                .setContentText(body)
////                .setSmallIcon(R.mipmap.ic_launcher);
////
////        notificationManager.notify(1, notification.build());
//    }

//    companion object {
//        //a public static string that will be used as the key
//        //for sending and receiving data
//        const val TASK_DESC = "task_desc"
//    }


    fun displayNotification(title:String,body:String) {
        val soundUri =
            Uri.parse("android.resource://" + applicationContext.packageName + "/" + com.namaztime.namaztime.R.raw.alarm)


        val intent = Intent(applicationContext, MainActivity::class.java)
        intent.flags = FLAG_ACTIVITY_NEW_TASK or FLAG_ACTIVITY_CLEAR_TASK
        intent.putExtra(NOTIFICATION_ID, id)

        val notificationManager =
            applicationContext.getSystemService(NOTIFICATION_SERVICE) as NotificationManager

      //  val bitmap = applicationContext.vectorToBitmap(R.drawable.ic_schedule_black_24dp)
        val titleNotification = title
        val subtitleNotification = body
        val pendingIntent = getActivity(applicationContext, 0, intent, 0)
        val notification = NotificationCompat.Builder(applicationContext, NOTIFICATION_CHANNEL)
            .setSmallIcon(R.drawable.alarm_immute)
            .setContentTitle(titleNotification).setContentText(subtitleNotification)
            .setDefaults(DEFAULT_ALL).setContentIntent(pendingIntent).setAutoCancel(true)

        notification.priority = PRIORITY_MAX

        if (SDK_INT >= O) {
            notification.setChannelId(NOTIFICATION_CHANNEL)

            val ringtoneManager = getDefaultUri(TYPE_NOTIFICATION)
            val audioAttributes = AudioAttributes.Builder().setUsage(USAGE_NOTIFICATION_RINGTONE)
                .setContentType(CONTENT_TYPE_SONIFICATION).build()

            val channel =
                NotificationChannel(NOTIFICATION_CHANNEL, NOTIFICATION_NAME, IMPORTANCE_HIGH)

            channel.enableLights(true)
            channel.lightColor = RED
            channel.enableVibration(true)
            channel.vibrationPattern = longArrayOf(100, 200, 300, 400, 500, 400, 300, 200, 400)
            channel.setSound(ringtoneManager, audioAttributes)
            notificationManager.createNotificationChannel(channel)
        }

        notificationManager.notify(0, notification.build())
    }

    companion object {
        const val NOTIFICATION_ID = "appName_notification_id"
        const val NOTIFICATION_NAME = "namaztime"
        const val NOTIFICATION_CHANNEL = "appName_channel_01"
        const val NOTIFICATION_WORK = "appName_notification_work"
    }

}
