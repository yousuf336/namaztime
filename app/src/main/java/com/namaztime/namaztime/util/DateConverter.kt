package com.namaztime.namaztime.util

import android.content.Context
import android.util.Log
import android.widget.Toast
import com.namaztime.namaztime.database.MySharedPreparence
import java.text.SimpleDateFormat
import java.util.*

class DateConverter {
    fun getIntervalBetweenTwoTimes(CurrentDate: String, FinalDate: String): String? {
        try {
//            val CurrentDate = "08/21/2020 11:26:50 am"
//            val FinalDate = "08/21/2020 11:29:40 am"
            val date1: Date
            val date2: Date
            val dates = SimpleDateFormat("MM/dd/yyyy hh:mm:ss a")
            date1 = dates.parse(CurrentDate)
            date2 = dates.parse(FinalDate)
            val difference = Math.abs(date1.time - date2.time)
            val differenceDates = difference / (24 * 60 * 60 * 1000)
            val differenceHours = difference / (1000 * 60 * 60)
            val differenceMinutes = difference % (1000 * 60 * 60)
            val dayDifference = java.lang.Long.toString(differenceDates)
          //  Log.e("timedifference", "${differenceHours}h:${differenceMinutes / (1000 * 60)}m")
            //  textView.setText("The difference between two dates is " + dayDifference + " days");
            return "${differenceHours}h:${differenceMinutes / (1000 * 60)}m"
        } catch (exception: Exception) {
            // Toast.makeText(ctx, "Unable to find difference", Toast.LENGTH_SHORT).show()
            Log.e("timedifference", "Unable to find difference")
        }
        return null;
    }

    fun getIntervalBetweenTwoTimesInMinutes(CurrentDate: String, FinalDate: String): Long {
        try {
//            val CurrentDate = "08/21/2020 11:26:50 am"
//            val FinalDate = "08/21/2020 11:29:40 am"
            val date1: Date
            val date2: Date
            val dates = SimpleDateFormat("MM/dd/yyyy hh:mm:ss a")
            date1 = dates.parse(CurrentDate)
            date2 = dates.parse(FinalDate)
            val difference = Math.abs(date1.time - date2.time)
            return (difference / (1000 * 60))
        } catch (exception: Exception) {
            // Toast.makeText(ctx, "Unable to find difference", Toast.LENGTH_SHORT).show()
            Log.e("timedifference", "Unable to find difference")
        }
        return 0;
    }

    fun getTimeInHoursMinutes(
        CurrentDate: String,
        FinalDate: String,
        preparence: MySharedPreparence?
    ): String? {
        try {
//            val CurrentDate = "08/21/2020 11:26:50 am"
//            val FinalDate = "08/21/2020 11:29:40 am"
            val date1: Date
            val date2: Date
            val dates = SimpleDateFormat("MM/dd/yyyy hh:mm:ss a")
            date1 = dates.parse(CurrentDate)
            date2 = dates.parse(FinalDate)
            val difference = Math.abs(date1.time - date2.time)
            val differenceDates = difference / (24 * 60 * 60 * 1000)
            val differenceHours = difference / (1000 * 60 * 60)
            val differenceMinutes = difference % (1000 * 60 * 60)
            val dayDifference = java.lang.Long.toString(differenceDates)
//            Log.e(
//                "timedifference",
//                "${Converter().numberEnglishToBanglaConvert(differenceHours.toInt())}h:${Converter().numberEnglishToBanglaConvert(
//                    (differenceMinutes / (1000 * 60)).toInt()
//                )}m"
//            )
            //  textView.setText("The difference between two dates is " + dayDifference + " days");
            if (differenceHours > 0)
                if (preparence?.getLanguage().equals(StaticKey().BANGLA))
                    return "${Converter().numberEnglishToBanglaConvert(differenceHours.toInt())}ঘণ্টা ${Converter().numberEnglishToBanglaConvert(
                        (differenceMinutes / (1000 * 60)).toInt()
                    )} মিনিটস"
                else
                    return "${differenceHours.toInt()}Hours ${(differenceMinutes / (1000 * 60)).toInt()} Minutes"
            else {
                if (preparence?.getLanguage().equals(StaticKey().BANGLA))
                    return "${Converter().numberEnglishToBanglaConvert((differenceMinutes / (1000 * 60)).toInt())} মিনিটস"
                else
                    return "${(differenceMinutes / (1000 * 60)).toInt()} Minutes"
            }
        } catch (exception: Exception) {
            // Toast.makeText(ctx, "Unable to find difference", Toast.LENGTH_SHORT).show()
            Log.e("timedifference", "Unable to find difference "+exception.message)
          //  Log.e("timedifference", "Unable to find difference")
        }
        return null
    }

}