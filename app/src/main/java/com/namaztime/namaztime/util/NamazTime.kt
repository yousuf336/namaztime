package com.namaztime.namaztime.util

import android.content.Context
import com.batoulapps.adhan.CalculationMethod
import com.batoulapps.adhan.Coordinates
import com.batoulapps.adhan.Madhab
import com.batoulapps.adhan.PrayerTimes
import com.batoulapps.adhan.data.DateComponents
import com.namaztime.namaztime.database.MySharedPreparence
import java.text.SimpleDateFormat
import java.util.*


class NamazTime {
    var context: Context? = null
    var preparence: MySharedPreparence? = null

    constructor(context: Context) {

        preparence = MySharedPreparence(context)
        this.context = context
    }

    fun namazeTimeToday(): PrayerTimes {

        val calendar = Calendar.getInstance()
        val today = calendar.time
        val coordinates = Coordinates(
            preparence?.getLatitude()?.toDouble()!!,
            preparence?.getLongitude()?.toDouble()!!
        )
        val dateComponents = DateComponents.from(Date())
        val parameters = CalculationMethod.KARACHI.parameters
        val dates = SimpleDateFormat("MM/dd/yyyy hh:mm:ss a")
        val formatter = SimpleDateFormat("hh:mm a")
        formatter.timeZone = TimeZone.getDefault()
        if (preparence?.getMadhab().equals(StaticKey().HANAFI))
            parameters.madhab = Madhab.HANAFI
        else
            parameters.madhab = Madhab.SHAFI
        val prayerTimes = PrayerTimes(coordinates, dateComponents, parameters)
        return prayerTimes
    }

    fun namazeTimeTomorrow(): PrayerTimes {
        val calendar = Calendar.getInstance()
        val today = calendar.time
        calendar.add(Calendar.DAY_OF_YEAR, 1)
        val tomorrow = calendar.time
        val coordinates = Coordinates(
            preparence?.getLatitude()?.toDouble()!!,
            preparence?.getLongitude()?.toDouble()!!
        )
        val dateComponentsTomorrow = DateComponents.from(tomorrow)
        val parameters = CalculationMethod.MUSLIM_WORLD_LEAGUE.parameters
        val dates = SimpleDateFormat("MM/dd/yyyy hh:mm:ss a")
        val formatter = SimpleDateFormat("hh:mm a")
        formatter.timeZone = TimeZone.getDefault()
        if (preparence?.getMadhab().equals(StaticKey().HANAFI))
            parameters.madhab = Madhab.HANAFI
        else
            parameters.madhab = Madhab.SHAFI
        val prayerTimesTomorrow = PrayerTimes(coordinates, dateComponentsTomorrow, parameters)
        return prayerTimesTomorrow
    }
}
