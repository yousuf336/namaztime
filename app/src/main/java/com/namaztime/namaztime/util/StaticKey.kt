package com.namaztime.namaztime.util

class StaticKey {
    val BANGLA="bn"
    val ENGLISH="en"
    val FAJAR = "fajr"
    val ZUHR = "dhuhr"
    val ASR = "asr"
    val SUNSET = "sunset"
    val MAGRIB = "maghrib"
    val ISHA = "isha"
    val KEY = "key"
    val HANAFI = "hanafi"
    val SHAFI = "shafi"
}