package com.namaztime.namaztime.util

import android.os.Build
import androidx.annotation.RequiresApi
import java.time.LocalDate
import java.time.chrono.HijrahChronology
import java.time.chrono.HijrahDate
import java.util.*

class Converter {
    fun getDayInBangla(day: String?): String? {
        var convertedDay: String? = null
        when (day) {
            "Sat" -> convertedDay = "শনিবার"
            "Sun" -> convertedDay = "রবিবার"
            "Mon" -> convertedDay = "সোমবার"
            "Tue" -> convertedDay = "মঙ্গলবার"
            "Wed" -> convertedDay = "বুধবার"
            "Thu" -> convertedDay = "বৃহস্পতিবার"
            "Fri" -> convertedDay = "শুক্রবার"
        }
        return convertedDay
    }

    fun getMonthInBangla(month: String?): String? {
        var convertedMonth: String? = null
        when (month) {
            "Jan" -> convertedMonth = "জানুয়ারী"
            "Feb" -> convertedMonth = "ফেব্রুয়ারি"
            "Mar" -> convertedMonth = "মার্চ"
            "Apr" -> convertedMonth = "এপ্রিল"
            "May" -> convertedMonth = "মে"
            "Jun" -> convertedMonth = "জুন"
            "Jul" -> convertedMonth = "জুলাই"
            "Aug" -> convertedMonth = "আগস্ট"
            "Sep" -> convertedMonth = "সেপ্টেম্বর"
            "Oct" -> convertedMonth = "অক্টোবর"
            "Nov" -> convertedMonth = "নভেম্বর"
            "Dec" -> convertedMonth = "ডিসেম্বর"
        }

        return convertedMonth
    }

    fun numberEnglishToBanglaConvert(number: Int): String? {
        var convertedNumber = ""
        val str = number.toString()
        for (i in 0 until str.length) {
            when (str[i]) {
                '0' -> convertedNumber += "০"
                '1' -> convertedNumber += "১"
                '2' -> convertedNumber += "২"
                '3' -> convertedNumber += "৩"
                '4' -> convertedNumber += "৪"
                '5' -> convertedNumber += "৫"
                '6' -> convertedNumber += "৬"
                '7' -> convertedNumber += "৭"
                '8' -> convertedNumber += "৮"
                '9' -> convertedNumber += "৯"
            }
        }
        return convertedNumber
    }

    fun getArabicMonthNameInBangla(count: Int?): String? {
        var arabicMonth: String? = null
        when (count) {
            0 -> arabicMonth = "মহররম"
            1 -> arabicMonth = "সফর"
            2 -> arabicMonth = "রবিউল আউয়াল"
            3 -> arabicMonth = "রবিউস সানি"
            4 -> arabicMonth = "জমাদিউল আউয়াল"
            5 -> arabicMonth = "জমাদিউস সানি"
            6 -> arabicMonth = "রজব"
            7 -> arabicMonth = "শা‘বান"
            8 -> arabicMonth = "রমজান"
            9 -> arabicMonth = "শাওয়াল"
            10 -> arabicMonth = "জ্বিলকদ"
            11 -> arabicMonth = "জ্বিলহজ্জ"
        }
        return arabicMonth
    }

    fun getArabicMonthNameInEnglish(count: Int?): String? {
        var arabicMonth: String? = null
        when (count) {
            0 -> arabicMonth = "al-Muḥarram"
            1 -> arabicMonth = "Ṣafar"
            2 -> arabicMonth = "Rabīʿ al-ʾAwwal"
            3 -> arabicMonth = "Rabīʿ ath-Thānī"
            4 -> arabicMonth = "Jumādā al-ʾAwwal"
            5 -> arabicMonth = "Jumādā ath-Thāniyah"
            6 -> arabicMonth = "Rajab"
            7 -> arabicMonth = "Shaʿbān"
            8 -> arabicMonth = "Ramaḍān"
            9 -> arabicMonth = "Shawwāl"
            10 -> arabicMonth = "Zū al-Qaʿdah"
            11 -> arabicMonth = "Zū al-Ḥijjah"
        }
        return arabicMonth
    }
}