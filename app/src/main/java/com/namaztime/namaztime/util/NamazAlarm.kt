package com.namaztime.namaztime.util

import android.content.Context
import android.util.Log
import com.batoulapps.adhan.PrayerTimes
import com.namaztime.namaztime.R
import com.namaztime.namaztime.database.MySharedPreparence
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit

class NamazAlarm {
    var ctx: Context? = null
    val dates = SimpleDateFormat("MM/dd/yyyy hh:mm:ss a")
    var namazTimeToday: PrayerTimes? = null
    var namazTimeTomorrow: PrayerTimes? = null
    var preparence: MySharedPreparence? = null;

    constructor(context: Context) {
        this.ctx = context
        namazTimeToday = NamazTime(ctx!!).namazeTimeToday()
        namazTimeTomorrow = NamazTime(ctx!!).namazeTimeTomorrow()
        preparence = MySharedPreparence(ctx!!)
    }

    fun setAlarmForFojor(isForToday: Boolean?) {
        if (preparence?.isAlarmForFajr()!!)
            cancelAlarmForFojor()

        preparence?.setFajr(true)
        var initialDelay: Long = DateConverter().getIntervalBetweenTwoTimesInMinutes(
            dates.format(Date()),
            dates.format(
                if (isForToday!!) {
                    namazTimeToday?.fajr
                } else {
                    namazTimeTomorrow?.fajr
                }
            )
        )
        Log.e(
            "message", "Fojor time difference is : " + DateConverter().getIntervalBetweenTwoTimes(
                dates.format(Date()),
                dates.format(
                    if (isForToday!!) {
                        namazTimeToday?.fajr
                    } else {
                        namazTimeTomorrow?.fajr
                    }
                )
            )
        )
        WorkmanagerControler().setOneTimeRequest(
            ctx!!,
            StaticKey().KEY,
            StaticKey().FAJAR,
            StaticKey().FAJAR,
            initialDelay,
            TimeUnit.MINUTES
        )

    }

    fun cancelAlarmForFojor() {
        preparence?.setFajr(false)
        preparence?.setAllAlarm(false)
        WorkmanagerControler().cacelRequestByTag(ctx!!, StaticKey().FAJAR)
        Log.e("message", "cancel alarm for fojor")
    }

    fun setAlarmForJohor(isForToday: Boolean?) {
        if (preparence?.isAlarmForDhuhr()!!)
            cancelAlarmForJohor()

        preparence?.setDhuhr(true)
        var initialDelay: Long = DateConverter().getIntervalBetweenTwoTimesInMinutes(
            dates.format(Date()),
            dates.format(
                if (isForToday!!) {
                    namazTimeToday?.dhuhr
                } else {
                    namazTimeTomorrow?.dhuhr
                }
            )
        )
        Log.e(
            "message", "johor time difference is : " + DateConverter().getIntervalBetweenTwoTimes(
                dates.format(Date()),
                dates.format(
                    if (isForToday!!) {
                        namazTimeToday?.dhuhr
                    } else {
                        namazTimeTomorrow?.dhuhr
                    }
                )
            )
        )
        WorkmanagerControler().setOneTimeRequest(
            ctx!!,
            StaticKey()
                .KEY,
            StaticKey().ZUHR,
            StaticKey().ZUHR,
            initialDelay,
            TimeUnit.MINUTES
        )
    }

    fun cancelAlarmForJohor() {
        preparence?.setDhuhr(false)
        preparence?.setAllAlarm(false)
        WorkmanagerControler().cacelRequestByTag(ctx!!, StaticKey().ZUHR)
        Log.e("message", "cancel alarm for johor")
    }

    fun setAlarmForAsr(isForToday: Boolean?) {
        if (preparence?.isAlarmForAsr()!!) {
            cancelAlarmForAsr()
        }

        preparence?.setAsr(true)
        var initialDelay: Long = DateConverter().getIntervalBetweenTwoTimesInMinutes(
            dates.format(Date()),
            dates.format(
                if (isForToday!!) {
                    namazTimeToday?.asr
                } else {
                    namazTimeTomorrow?.asr
                }
            )
        )
        Log.e(
            "message", "asr time difference is : " + DateConverter().getIntervalBetweenTwoTimes(
                dates.format(Date()),
                dates.format(
                    if (isForToday!!) {
                        namazTimeToday?.asr
                    } else {
                        namazTimeTomorrow?.asr
                    }
                )
            )
        )
        Log.e("message", "current payer " + namazTimeToday?.currentPrayer())
        WorkmanagerControler().setOneTimeRequest(
            ctx!!,
            StaticKey()
                .KEY,
            StaticKey().ASR,
            StaticKey().ASR,
            initialDelay,
            TimeUnit.MINUTES
        )


    }

    fun cancelAlarmForAsr() {
        preparence?.setAsr(false)
        preparence?.setAllAlarm(false)
        WorkmanagerControler().cacelRequestByTag(ctx!!, StaticKey().ASR)
        Log.e("message", "cancel alarm for asr")
    }

    fun setAlarmForSunSet(isForToday: Boolean?) {
        if (preparence?.isAlarmForSunset()!!) {
            cancelAlarmForSunSet()
        }
        preparence?.setSunset(true)
        var initialDelay: Long = DateConverter().getIntervalBetweenTwoTimesInMinutes(
            dates.format(Date()),
            dates.format(
                if (isForToday!!) {
                    namazTimeToday?.maghrib
                } else {
                    namazTimeTomorrow?.maghrib
                }
            )
        )
        Log.e(
            "message", "sunset time difference is : " + DateConverter().getIntervalBetweenTwoTimes(
                dates.format(
                    Date()
                ),
                dates.format(
                    if (namazTimeToday?.maghrib!! > Date()) {
                        namazTimeToday?.maghrib
                    } else {
                        namazTimeTomorrow?.maghrib
                    }
                )
            )
        )
        WorkmanagerControler().setOneTimeRequest(
            ctx!!,
            StaticKey()
                .KEY,
            StaticKey().SUNSET,
            StaticKey().SUNSET,
            initialDelay,
            TimeUnit.MINUTES
        )
    }

    fun cancelAlarmForSunSet() {
        preparence?.setSunset(false)
        preparence?.setAllAlarm(false)
        // Toast.makeText(this,"${preparence?.isAlarmForSunset()}",Toast.LENGTH_LONG).show()
        WorkmanagerControler().cacelRequestByTag(ctx!!, StaticKey().SUNSET.toString())
        Log.e("message", "cancel alarm for sunset")
    }

    fun setAlarmForMagribPrayer(isForToday: Boolean?) {
        if (preparence?.isAlarmForMagrib()!!) {
            cancelAlarmForMagrib()
        }
        preparence?.setMagrib(true)
        var initialDelay: Long = DateConverter().getIntervalBetweenTwoTimesInMinutes(
            dates.format(Date()),
            dates.format(
                if (isForToday!!) {
                    namazTimeToday?.maghrib
                } else {
                    namazTimeTomorrow?.maghrib
                }
            )
        )
        Log.e(
            "message", "magrib time difference is : " + DateConverter().getIntervalBetweenTwoTimes(
                dates.format(Date()),
                dates.format(
                    if (namazTimeToday?.maghrib!! > Date()) {
                        namazTimeToday?.maghrib
                    } else {
                        namazTimeTomorrow?.maghrib
                    }
                )
            )
        )
        WorkmanagerControler().setOneTimeRequest(
            ctx!!,
            StaticKey().KEY,
            StaticKey().MAGRIB,
            StaticKey().MAGRIB,
            initialDelay,
            TimeUnit.MINUTES
        )
    }

    fun cancelAlarmForMagrib() {
        preparence?.setMagrib(false)
        preparence?.setAllAlarm(false)
        WorkmanagerControler().cacelRequestByTag(ctx!!, StaticKey().MAGRIB.toString())
        Log.e("message", "cancel alarm for Magrib")
    }

    fun setAlarmForIsha(isForToday: Boolean?) {
        if (preparence?.isAlarmForIsha()!!) {
            cancelAlarmForIsha()
        }
        preparence?.setIsha(true)
        var initialDelay: Long = DateConverter().getIntervalBetweenTwoTimesInMinutes(
            dates.format(Date()),
            dates.format(
                if (isForToday!!) {
                    namazTimeToday?.isha
                } else {
                    namazTimeTomorrow?.isha
                }
            )
        )
        Log.e(
            "message", "Isha time difference is : " + DateConverter().getIntervalBetweenTwoTimes(
                dates.format(Date()),
                dates.format(
                    if (isForToday!!) {
                        namazTimeToday?.isha
                    } else {
                        namazTimeTomorrow?.isha
                    }
                )
            )
        )
        WorkmanagerControler().setOneTimeRequest(
            ctx!!,
            StaticKey()
                .KEY,
            StaticKey().ISHA,
            StaticKey().ISHA,
            initialDelay,
            TimeUnit.MINUTES
        )
    }

    fun cancelAlarmForIsha() {
        preparence?.setIsha(false)
        preparence?.setAllAlarm(false)
        WorkmanagerControler().cacelRequestByTag(ctx!!, StaticKey().ISHA.toString())
        Log.e("message", "cancel alarm for Isha")
    }

    fun refreshAllSelectedAlarm(currentPrayer: String?) {
        if (preparence?.isAlarmForFajr()!! && !currentPrayer.equals(StaticKey().FAJAR)!!) {
            NamazAlarm(this.ctx!!).setAlarmForFojor(
                if (namazTimeToday?.fajr!! > Date()) {
                    true
                } else {
                    false
                }
            )
        }
        if (preparence?.isAlarmForDhuhr()!! && !currentPrayer.equals(StaticKey().ZUHR)!!) {
            NamazAlarm(this.ctx!!).setAlarmForJohor(
                if (namazTimeToday?.dhuhr!! > Date()) {
                    true
                } else {
                    false
                }
            )
        }
        if (preparence?.isAlarmForAsr()!! && !currentPrayer.equals(StaticKey().ASR)!!) {
            NamazAlarm(this.ctx!!).setAlarmForAsr(
                if (namazTimeToday?.asr!! > Date()) {
                    true
                } else {
                    false
                }
            )
        }
        if (preparence?.isAlarmForSunset()!! && !currentPrayer.equals(StaticKey().SUNSET)!!) {
            NamazAlarm(this.ctx!!).setAlarmForSunSet(
                if (namazTimeToday?.maghrib!! > Date()) {
                    true
                } else {
                    false
                }
            )
        }
        if (preparence?.isAlarmForMagrib()!! && !currentPrayer.equals(StaticKey().MAGRIB)!!) {
            NamazAlarm(this.ctx!!).setAlarmForMagribPrayer(
                if (namazTimeToday?.maghrib!! > Date()) {
                    true
                } else {
                    false
                }
            )
        }
        if (preparence?.isAlarmForIsha()!! && !currentPrayer.equals(StaticKey().ISHA)!!) {
            NamazAlarm(this.ctx!!).setAlarmForIsha(
                if (namazTimeToday?.isha!! > Date()) {
                    true
                } else {
                    false
                }
            )
        }
    }
}