package com.namaztime.namaztime.util

import android.content.Context
import androidx.work.Data
import androidx.work.OneTimeWorkRequest
import androidx.work.WorkManager
import com.namaztime.namaztime.service.MyWorker
import java.util.concurrent.TimeUnit

class WorkmanagerControler {

    fun setOneTimeRequest(
        context: Context,
        dataKey: String,
        value: String,
        taskTag: String,
        time: Long,
        timeUnit: TimeUnit
    ) {
        val data = Data.Builder()
        data.putString(dataKey, value)
        val compressionWork = OneTimeWorkRequest.Builder(MyWorker::class.java)
            .setInitialDelay(time, timeUnit)
            .addTag(taskTag)
            .setInputData(data.build())
            .build()
        WorkManager.getInstance(context).enqueue(compressionWork)
    }

    fun cacelRequestByTag(context: Context, taskTag: String) {
        WorkManager.getInstance(context).cancelAllWorkByTag(taskTag)
    }
}